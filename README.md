Tyler Moffitt is a family law and criminal law attorney handling divorce, child support and alimony cases as well as DUI cases in Carrollton, LaGrange and Columbus. Moffitt Law is an authority on family and criminal law in West Georgia, so call them today to see your family law or criminal law case.

Address: 202 Bradley Street, Suite E9, Carrollton, GA 30117, USA
Phone: 762-212-3951
